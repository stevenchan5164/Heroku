//https://ctrlq.org/code/20020-query-book-by-isbn
var restify = require("restify");
var request = require("request");
var url = require("url");
var rand = require("csprng");
var sha1 = require("sha1");
var firebase = require('firebase-admin');
var id = 0;
//var APIkey="AIzaSyCQHXxRz5XAqGxm2pLDVKh0MrBeCaLQdgk"
var jsondata;
var data;

firebase.initializeApp({
  credential: firebase.credential.cert({
    projectId: 'probable-axon-136323',
    clientEmail: 'firebase-adminsdk-l5fi5@probable-axon-136323.iam.gserviceaccount.com',
    privateKey: '-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCPts7ma2o6eVWq\nKCzwzDr1pEmYndlFC1idbagAIaorWDJfJDgQnU2LAMw64U1MhPbOGNDNMXiEdqxh\nTxkj4J0RJ/8HwvifUe5VxXP5bk5c9+XofZlugoQlGty72HaDaVnhb5EPNKilt4yv\nWA1Rj8EieWXDlC4yG1I4xeLkyRXeo2jSCn+cizhm13U7d1SgGx/KESdm4j6thbvY\n63MvnQ+QmtKbnRzFOGhnjbloeO6/0kEH8jPZPkshCe+tZSf5IlfV5S8wwO+SCsUS\n+XAcmQjgcyCg3rf09wj1+YXTlV9SF7/aI+FAp8jS99lpXQoa1PAgwPqnhEInKxjj\n2rwufu49AgMBAAECgf8fjJvfLOi+ukqtqb3NhW6fSz52dco8xHQWG2L5cTdQswv1\ncJerDhCRc2gvHnoM6/B1tJaIdVPMjXQXG9/PZuZHd4BSAK6T5M8HOITW1sbDog6j\nlO8Z7Sau/p0QOXT+MSKKJ8Cwld7WgnT5ovR8wBI/smbX7xY1HP2JeRrv65t7EUOG\n5QwyxyExihKiH+vvV6SWe1QTT6LiqrR9juYSZgl6ydYTm9TFTkroDm0te4qsl4/M\niWk3z1QnskPPHpkHaCn3ljK+7rfNn284VMR/2f1GVBs7cxfIPpgdVMnZR4j/Gyf5\nOgKQOWfaAG5TTvEBYKlRkPYWaJN05Cjv7qqPDAECgYEAwJviX8jupiSda1vMMAvW\ndJveKMzT89zofaesi2etiY+t7h6uSIe3crPKZ1gNeeQ8+0A19ceRwyXXxh3KMEe7\nE5Dla4p5oT+72utloCJea16BVSlVfM20YLqyh1IWlCtpkDmvSR9W4qQNnKS86LU9\nGmZHEPQUrX2A4tqP+AqfaQECgYEAvwNT+DpxK3FJOV85eB9dXg+RNl6oqKrK1oeB\ndcPZKaIP5RpWMiuR+xzDoEIHnDqh3Q1vq5lSlOU3iaZzwxaBuBbC4gVXafICN2Wv\nMyQMqMalWq180VQvogpr4Ih5Zpd16u/LN5s8/1jJ+a8JgXK3arMP3oYFnJDCu9QI\nPrbx6T0CgYEAuvFSCXfjsdxN8hq6F1QWBT4XpXAQtFuQSA2LWg76D08mGL0smXco\nZar9Y8rB4bHWQmKzPOdDoa5EPKVDThMBD1+OXQ+dOBW9BiF9lKxnCj1CuF6S+7xI\nO65Zgx/4jD2KixKCAC3rzbQ/Be+a7x6hvNLSXTEaNL1gcE6Ed9IPoQECgYAUvvnJ\nxh8whrdbQdpuD4oGg05UR2euGg65yjHnZoQZn352davS+yR5z5/3sfQ9paia57Cv\nRJHRliu4CZodID6qjd5Qyh+6ZAyVk7e5qDqvwhOHi7w4yBn8UYFx+6Cj4eNuxuid\ne4Degvg85CP2KU1+i0A3/PpzDM35tIfIt/oCSQKBgQCfUOjf4+02DBpgeKwcB6+o\nFBSuuVnsz/DNGOBeQE5CXpe/AdJ1ukkGey+ggUCDXt11qkXCGGNo3bEq3g6vFAzb\nIxYlpJNIzpMOFluPJXnzZLA1tOMc34gR5Zbz3N5BDi/0jooOLSuJKAadiUSXNVoj\nx2zJVmCMTU0laRoz7rnCxQ==\n-----END PRIVATE KEY-----\n'
  }),
  databaseURL: 'https://probable-axon-136323.firebaseio.com'
});

/*
var config = {
        apiKey: "AIzaSyDU2XMGXBsGjIlA8pCEGy-FnfpDr88GhXs",
        authDomain: "probable-axon-136323.firebaseapp.com",
        databaseURL: "https://probable-axon-136323.firebaseio.com",
        projectId: "probable-axon-136323",
        storageBucket: "probable-axon-136323.appspot.com",
        messagingSenderId: "291064551399"
    };
    
firebase.initializeApp(config);
*/

var server = restify.createServer();
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser());
server.use(restify.plugins.authorizationParser());
var ref = firebase.database().ref().child('books');
var logref = ref.child('1');

ref.once('value', function(snap){
    //console.log(snap.val());
    data = snap.val();
});

function writeBookData(title, authors, subtitlem, isbn) {
    firebase.database().ref('books/' + isbn).set({
        title: title,
        authors: authors,
        subtitlem: subtitlem,
        lend: "false"
    });
}

function updateBookData(isbn) {
    var checking = firebase.database().ref('books/' + isbn).child('lend');
        
    checking.once('value', function(snap){
        var lend = snap.val();
        
        if(lend == "false"){
            firebase.database().ref('books/' + isbn).child('lend').set("true");
        }else{
            firebase.database().ref('books/' + isbn).child('lend').set("false");
        }
    });
}

function deleteBookData(isbn) {
    firebase.database().ref('books/' + isbn).remove();
}

server.listen(8080, function(){
   console.log("incoming request being handled");
   
    server.get('/getall', function(req, res, next) {
        
        res.send(data);
        res.end();
    });
    
    server.get('/get', function(req, res, next) {
        
        var input = url.parse(req.url, true).query;
        var data = firebase.database().ref('books/' + input.isbn);
            
        data.once('value', function(snap){
            var output = snap.val();
                
            if (output != null){
                res.send(output);
                res.end();
            }else{
                res.send("ISBN error");
                res.end();
            }
        });   
    });
    
    server.post('/', function(req, res){
        
        var isbn = req.body.isbn;
        var url = "https://www.googleapis.com/books/v1/volumes?q=isbn:" + isbn;
        
        request({
            url: url,
            json: true
        }, function (error, response, body) {

        if (body.items) {
                jsondata = body; // Print the json response
                //var obj = JSON.parse(body);
                var title = body.items[0].volumeInfo.title;
                var authors = body.items[0].volumeInfo.authors[0];
                
                if(body.items[0].volumeInfo.subtitle){
                    var subtitle = body.items[0].volumeInfo.subtitle;
                }else{
                    var subtitle = "Nil";
                }
                
                writeBookData(title,authors,subtitle,isbn);
                //console.log(body.items[0].volumeInfo);
                res.send("POST End");
                res.end();
            }else{
                jsondata = "Nil";
                //console.log("Nil");
                res.send("Book added");
                res.end();
            }
        });
    });
    
    server.put('/', function(req, res, next) {
        
        var isbn = req.body.isbn;
        //updateBookData(isbn);
        updateBookData(isbn);
        res.send("Book updated");
        res.end();
    });
    
    server.del('/', function(req, res, next) {
        
        var isbn = req.body.isbn;
        //updateBookData(isbn);
        deleteBookData(isbn);
        res.send("Book deleted");
        res.end();
    });
    
});
